package com.twuc.webApp.domain.simple;

// TODO:
//
// 请创建一个 UserProfile entity。其对应的数据库表的 schema 必须满足如下 SQL 的要求：
//
// table: user_profile
// +────────────────+──────────────+───────────────────────────────+
// | column         | description  | additional                    |
// +────────────────+──────────────+───────────────────────────────+
// | id             | bigint       | auto_increment, primary key   |
// | name           | varchar(64)  | not null                      |
// | year_of_birth  | smallint     | not null                      |
// +────────────────+──────────────+───────────────────────────────+
//
//  请补全如下的代码，你可以新建各种方法、构造器，添加各种 annotation。但是添加的东西越少越好。
//
// <--start-


import org.springframework.lang.NonNull;

import javax.persistence.*;

@Entity
public class UserProfile {
    public static UserProfile of(String name, short yearOfBirth) {
        return new UserProfile(name, yearOfBirth);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(nullable = false,length = 64)
    String name;
    @Column(nullable = false)
    Short year_of_birth;

    public UserProfile() {
    }

    private UserProfile(String name, Short yearOfBirth) {
        // 请实现该构造函数
        this.name = name;
        this.year_of_birth = yearOfBirth;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getYearOfBirth() {
        return year_of_birth;
    }

    public void setYearOfBirth(Short year_of_birth) {
        this.year_of_birth = year_of_birth;
    }
}
// --end-->