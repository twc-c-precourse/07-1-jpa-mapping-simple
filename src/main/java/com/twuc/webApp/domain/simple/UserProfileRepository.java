package com.twuc.webApp.domain.simple;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;


@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {


    @Modifying
    @Query("update UserProfile u set u.name=?1 where u.name=?2")
    void updateUserProfileByName(String newame, String oldname);
}
