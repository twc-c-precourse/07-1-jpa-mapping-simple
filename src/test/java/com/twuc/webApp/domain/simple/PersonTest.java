package com.twuc.webApp.domain.simple;

import com.twuc.webApp.domain.simple.Person;
import com.twuc.webApp.domain.simple.PersonRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class PersonTest {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    void should_save_person() {
        Person expect = new Person(1, "shuang", "kou");
        personRepository.save(expect);

        Optional<Person> actual = personRepository.findById(1);
        entityManager.flush();
        entityManager.clear();
        if (actual.isPresent()) {
            assertEquals(actual.get().getFirstName(), "shuang");
        }


    }
}
